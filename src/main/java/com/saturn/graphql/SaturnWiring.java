//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.saturn.graphql;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saturn.graphql.data.DynamicDataFetcher;
import com.saturn.graphql.util.CommonUtil;
import org.dataloader.BatchLoader;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderRegistry;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SaturnWiring {
    private static final Logger LOGGER = Logger.getLogger(SaturnWiring.class.getName());
    public static JsonNode manifest;
    private static BatchLoader<String, Object> dynamicBatchLoader = (uri) ->
          CompletableFuture.supplyAsync(() -> getDataByRestHttpApi(uri));



    private static List<Object> getDataByRestHttpApi(List<String> uri) {
        return uri.stream().map(CommonUtil::executeRestCall).collect(Collectors.toList());

    }

    private static DataLoader<String, Object> newDymanicDataLoader() {
        return new DataLoader<>(dynamicBatchLoader);
    }

    public static void main(String[] args) {
        String uri = "http://localhost:8082/rest/portfolios";
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(uri);
        Builder requestBuilder = webTarget.request(new String[]{"application/json"});
        Response response = requestBuilder.get();
        List<Map<String, String>> list = null;
        if (response.getStatus() == 200) {
            String responseBody = (String)response.readEntity(String.class);
            ObjectMapper mapper = new ObjectMapper();

            try {
                list = (List)mapper.readValue(responseBody, new TypeReference<List<Map<String, String>>>() {
                });
            } catch (Exception var10) {
                LOGGER.log(Level.SEVERE, "Error while formatting message");
            }
        }

        LOGGER.log(Level.INFO, "Output:[" + list + "]");
    }

    public static class Context {
        final DataLoaderRegistry dataLoaderRegistry = new DataLoaderRegistry();

        public Context() {
            this.dataLoaderRegistry.register("dataloader", SaturnWiring.newDymanicDataLoader());
        }

        public DataLoaderRegistry getDataLoaderRegistry() {
            return this.dataLoaderRegistry;
        }

        public DataLoader<String, Object> getDynamicDataLoader() {
            return this.dataLoaderRegistry.getDataLoader("dataloader");
        }
    }



}
