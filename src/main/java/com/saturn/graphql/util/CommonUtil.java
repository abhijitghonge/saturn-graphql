//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.saturn.graphql.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommonUtil {
    private static final Logger LOGGER = Logger.getLogger(CommonUtil.class.getName());

    private CommonUtil() {
    }

    public static String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, "utf8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException var2) {
            return str;
        }
    }

    /**
     * Creates a rest client and makes a rest call
     * @param uri
     * @return
     */
    public static List<Map<String, String>> executeRestCall(String uri){

        LOGGER.log(Level.INFO, "Received URI:["+uri+"]");
        Client client = ClientBuilder.newClient();

        WebTarget webTarget = client.target(uri);
        Invocation.Builder requestBuilder = webTarget.request();
        Response response = requestBuilder.get();
        List<Map<String, String>> data = null;
        if (response.getStatus() == 200) {
            String responseBody = response.readEntity(String.class);
            ObjectMapper mapper = new ObjectMapper();

            try {
                data = mapper.readValue(responseBody, new TypeReference<List<Map<String, String>>>() {
                });
            } catch (Exception var13) {
                LOGGER.log(Level.SEVERE, "Error while formatting message:[" + var13.getMessage() + "]");
                var13.printStackTrace();
            }
        }

        LOGGER.log(Level.INFO, "Output:[" + data + "]");
        return data;

    }

    public static Date parseDate(String dateString, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date date = null;

        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException var5) {
            LOGGER.log(Level.SEVERE, "Invalid date format");
        }

        return date;
    }

    public static Map<String, String> convert(Map<String, Object> objectMap) {
        Map<String, String> expected = new LinkedHashMap(objectMap.size());
        Iterator var2 = objectMap.entrySet().iterator();

        while(var2.hasNext()) {
            Entry<String, Object> objectEntry = (Entry)var2.next();
            expected.put(objectEntry.getKey(), objectEntry.getValue().toString());
        }

        return expected;
    }

    public static Date parseDate(String dateString) {
        return parseDate(dateString, "yyyy-mm-dd");
    }
}
