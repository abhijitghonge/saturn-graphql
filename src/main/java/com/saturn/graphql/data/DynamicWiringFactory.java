//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.saturn.graphql.data;

import graphql.language.FieldDefinition;
import graphql.schema.DataFetcher;
import graphql.schema.idl.FieldWiringEnvironment;
import graphql.schema.idl.WiringFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DynamicWiringFactory implements WiringFactory {
    private RestAPI rest;
    private static final Logger LOGGER = Logger.getLogger(DynamicWiringFactory.class.getName());

    public DynamicWiringFactory(RestAPI restAPI) {
        this.rest = restAPI;
    }

    public boolean providesDataFetcher(FieldWiringEnvironment environment) {
        FieldDefinition fieldDefinition = environment.getFieldDefinition();
        LOGGER.log(Level.INFO, "Field Definition:[" + fieldDefinition + "]");
        return this.rest.getRestDetailsMap().containsKey(fieldDefinition.getName());
    }

    public DataFetcher getDataFetcher(FieldWiringEnvironment environment) {
        FieldDefinition fieldDefinition = environment.getFieldDefinition();
        String operationId = fieldDefinition.getName();
        String endpoint = (String)this.rest.getRestDetailsMap().get(operationId);
        String baseUri = this.rest.getUrl();
        return new DynamicDataFetcher(operationId, endpoint, baseUri);
    }
}
