//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.saturn.graphql.data;

import com.saturn.graphql.SaturnWiring;
import com.saturn.graphql.util.CommonUtil;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLArgument;
import graphql.schema.GraphQLFieldDefinition;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class DynamicDataFetcher implements DataFetcher {
    private static final Logger LOGGER = Logger.getLogger(DynamicDataFetcher.class.getName());
    private String operationId;
    private String endpoint;
    private String baseUri;

    public DynamicDataFetcher(String operationId, String endpoint, String baseUri) {
        this.operationId = operationId;
        this.endpoint = endpoint;
        this.baseUri = baseUri;
    }

    public Object get(DataFetchingEnvironment environment) {

        SaturnWiring.Context context =  environment.getContext();
        Object source = environment.getSource();

        String endpointUri = this.baseUri + this.endpoint;
        if (source != null) {
            endpointUri = prepareEndpointUri(environment,endpointUri);
        }

        // In case the call is with arguments from the graphql query
        Map<String, Object> arguments = environment.getArguments();
        if (arguments != null) {
            endpointUri = this.appendQueryParams(endpointUri, CommonUtil.convert(arguments));
        }

        LOGGER.log(Level.INFO, "The URI:[" + endpointUri + "]");

        return context.getDynamicDataLoader().load(endpointUri);
    }

    /**
     * Given an environement, this method will prepare the rest API URL by extracting
     * the parent context
     * e.g. Query:
     * {
     *     positions{
     *         portfolioId
     *         security{
     *             sedol
     *             symbol
     *         }
     *     }
     * }
     * @param environment
     * @return
     */
    private String prepareEndpointUri(DataFetchingEnvironment environment, String baseUri){
        //Get the field definition by operation Id/rest endpoint
        GraphQLFieldDefinition fieldDefinition = environment.getGraphQLSchema()
                .getQueryType().getFieldDefinition(this.operationId);
        //Inspect GraphQl Schema for expected arguments by the rest endpoint
        List<GraphQLArgument> inputParams = fieldDefinition.getArguments();
        List<String> argumentNames = this.names(inputParams);

        //From the parent Context filter the required arguments by end points
        Map<String, String> parentContext = (LinkedHashMap)environment.getSource();
        Map<String, String> arguments = parentContext.entrySet().stream().filter((entry) -> {
            return argumentNames.contains(entry.getKey());
        }).collect(Collectors.toMap((entry) -> {
            return (String)entry.getKey();
        }, (entry) -> {
            return (String)entry.getValue();
        }));

        //Prepare the endpoint URI
        return this.appendQueryParams(baseUri, arguments);
    }

    private List<String> names(List<GraphQLArgument> arguments) {
        List<String> argumentNames = new ArrayList(arguments.size());
        Iterator var3 = arguments.iterator();

        while(var3.hasNext()) {
            GraphQLArgument argument = (GraphQLArgument)var3.next();
            argumentNames.add(argument.getName());
        }

        return argumentNames;
    }

    private String appendQueryParams(String path, Map<String, String> queryParamValuesMap) {
        StringBuilder sb = new StringBuilder();
        sb.append(path);
        String prefix = path.contains("?") ? "&" : "?";
        Iterator var5 = queryParamValuesMap.keySet().iterator();

        while(var5.hasNext()) {
            String key = (String)var5.next();
            String value = (String)queryParamValuesMap.get(key);
            value = value.replaceAll("\"", "");
            if (value != null && !"".equals(value)) {
                if (prefix != null) {
                    sb.append(prefix);
                    prefix = null;
                } else {
                    sb.append("&");
                }

                sb.append(key).append("=").append(CommonUtil.urlEncode(value));
            }
        }

        return sb.toString();
    }
}
