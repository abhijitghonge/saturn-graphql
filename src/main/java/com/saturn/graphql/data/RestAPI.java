//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.saturn.graphql.data;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class RestAPI {
    String baseUri;
    Map<String, String> restDetailsMap = new HashMap();

    public RestAPI(JsonNode manifest) {
        JsonNode endpointNode = manifest.path("paths");
        Iterator<Entry<String, JsonNode>> endpointiterator = endpointNode.fields();
        new HashMap();

        String basePath;
        String scheme;
        while(endpointiterator.hasNext()) {
            Entry<String, JsonNode> endpointEntry = (Entry)endpointiterator.next();
            basePath = (String)endpointEntry.getKey();
            scheme = ((JsonNode)endpointEntry.getValue()).path("operationId").textValue();
            this.restDetailsMap.put(scheme, basePath);
        }

        String host = manifest.path("hostport").textValue();
        basePath = manifest.path("basePath").textValue();
        scheme = "http";
        this.baseUri = scheme + "://" + host + basePath;
    }

    public Map<String, String> getRestDetailsMap() {
        return this.restDetailsMap;
    }

    public String getUrl() {
        return this.baseUri;
    }
}
