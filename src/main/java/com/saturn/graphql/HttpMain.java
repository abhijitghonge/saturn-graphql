//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.saturn.graphql;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.saturn.graphql.SaturnWiring.Context;
import com.saturn.graphql.data.DynamicDataFetcher;
import com.saturn.graphql.data.DynamicWiringFactory;
import com.saturn.graphql.data.RestAPI;
import com.saturn.graphql.util.JsonKit;
import com.saturn.graphql.util.QueryParameters;
import graphql.ExecutionInput;
import graphql.ExecutionInput.Builder;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.execution.instrumentation.ChainedInstrumentation;
import graphql.execution.instrumentation.Instrumentation;
import graphql.execution.instrumentation.dataloader.DataLoaderDispatcherInstrumentation;
import graphql.execution.instrumentation.dataloader.DataLoaderDispatcherInstrumentationOptions;
import graphql.execution.instrumentation.tracing.TracingInstrumentation;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.dataloader.DataLoaderRegistry;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;

public class HttpMain extends AbstractHandler {
    static final int PORT = 3000;
    static GraphQLSchema dynamicSchema = null;

    public HttpMain() {
    }

    public static void main(String[] args) throws Exception {
        Server server = new Server(3000);
        HttpMain main_handler = new HttpMain();
        ResourceHandler resource_handler = new ResourceHandler();
        resource_handler.setDirectoriesListed(false);
        resource_handler.setWelcomeFiles(new String[]{"index.html"});
        resource_handler.setResourceBase("./src/main/resources/httpmain");
        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{resource_handler, main_handler});
        server.setHandler(handlers);
        server.start();
        server.join();
    }

    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if ("/graphql".equals(target)) {
            baseRequest.setHandled(true);
            this.handleQuery(request, response);
        }

    }

    private void handleQuery(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        QueryParameters parameters = QueryParameters.from(httpRequest);
        if (parameters.getQuery() == null) {
            httpResponse.setStatus(400);
        } else {
            Builder executionInput = ExecutionInput
                    .newExecutionInput()
                    .query(parameters.getQuery())
                    .operationName(parameters.getOperationName())
                    .variables(parameters.getVariables());

            //
            // the context object is something that means something to down stream code.  It is instructions
            // from yourself to your other code such as DataFetchers.  The engine passes this on unchanged and
            // makes it available to inner code
            //
            // the graphql guidance says  :
            //
            //  - GraphQL should be placed after all authentication middleware, so that you
            //  - have access to the same session and user information you would in your
            //  - HTTP endpoint handlers.
            //
            Context context = new Context();

            executionInput.context(context);

            // you need a schema in order to execute queries
            GraphQLSchema schema = this.buildDynamicSchema();

            // This example uses the DataLoader technique to ensure that the most efficient
            // loading of data (in this case StarWars characters) happens.  We pass that to data
            // fetchers via the graphql context object.
            //

            DataLoaderRegistry dataLoaderRegistry = context.getDataLoaderRegistry();
            DataLoaderDispatcherInstrumentation dlInstrumentation = new DataLoaderDispatcherInstrumentation(dataLoaderRegistry, DataLoaderDispatcherInstrumentationOptions
                    .newOptions()
                    .includeStatistics(true));
            Instrumentation instrumentation = new ChainedInstrumentation(Arrays.asList(new TracingInstrumentation(), dlInstrumentation));
            GraphQL graphQL = GraphQL.newGraphQL(schema)
                    .instrumentation(instrumentation).build();
            ExecutionResult executionResult = graphQL.execute(executionInput.build());
            this.returnAsJson(httpResponse, executionResult);
        }
    }

    private void returnAsJson(HttpServletResponse response, ExecutionResult executionResult) throws IOException {
        response.setContentType("application/json");
        response.setStatus(200);
        JsonKit.toJson(response, executionResult.toSpecification());
    }

    private GraphQLSchema buildDynamicSchema() throws IOException {
        if (dynamicSchema == null) {
            Reader streamReader = this.loadFile("portfolio.graphqls");
            TypeDefinitionRegistry typeRegistry = (new SchemaParser()).parse(streamReader);

            //Load the descriptor for manifest.
            //TODO: One to On mapping of YAML to datafetcher, since datafectcher will be dedicated to that micro service
            Reader manifestReader = this.loadFile("portfolio.yml");
            YAMLFactory yamlFactory = new YAMLFactory();
            JsonParser parser = yamlFactory.createParser(manifestReader);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = (JsonNode) mapper.readTree(parser);
            SaturnWiring.manifest = rootNode;
            RestAPI restDetails = new RestAPI(rootNode);

            //Need to use dynamic wiring factory to provide data fetcher dynamically
            RuntimeWiring.Builder builder = RuntimeWiring
                    .newRuntimeWiring()
                    .wiringFactory(new DynamicWiringFactory(restDetails));

            //Create a data fetcher for every operation mentioned in YAML since these are individual queries
            restDetails.getRestDetailsMap().forEach((operationId, endpoint) -> {
                builder.type("Query", (typeWiring) -> {
                    return typeWiring.dataFetcher(operationId, new DynamicDataFetcher(operationId, endpoint, restDetails.getUrl()));
                });
            });
            RuntimeWiring wiring = builder.build();
            dynamicSchema = (new SchemaGenerator()).makeExecutableSchema(typeRegistry, wiring);
        }

        return dynamicSchema;
    }

    private Reader loadFile(String name) {
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream(name);
        return new InputStreamReader(stream);
    }
}
